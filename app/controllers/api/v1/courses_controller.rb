class Api::V1::CoursesController < ApplicationController
  def index
    @courses = Course.all
    render json: @courses
  end

  def new
    @course = Course.new
  end

  def create
    @course = Course.create(course_params)
    render json: @course
  end

  def show
    @course = Course.find(params[:id])
    render json: @course
  end

  def edit
    @course = Course.find(params[:id])
  end

  def update
    @course = Course.find(params[:id])
    @course.update
  end

  def destroy
    @course = Course.find(params[:id])
    @course.destroy
  end

  private

  def course_params
    params.require(:course).permit(:course_name, :course_number, :section_number, :term, :instructor)
  end

end
