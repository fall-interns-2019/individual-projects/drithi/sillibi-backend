class AssignmentSerializer < ActiveModel::Serializer
  # belongs_to :course
  # belongs_to :user, through: :course
  attributes :id, :assignment_name, :due_date, :description, :grade
end
